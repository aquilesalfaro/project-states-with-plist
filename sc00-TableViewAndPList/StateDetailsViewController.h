//
//  StateDetailsViewController.h
//  sc00-TableViewAndPList
//
//  Created by Orlando Gotera on 11/7/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatesTableViewController.h"
@interface StateDetailsViewController : UIViewController
@property (strong, nonatomic) NSString * myStateName;
@property (strong, nonatomic) NSString * myStateCapital;
@property (strong, nonatomic) NSString * myStateMotto;
@property (strong, nonatomic) NSString * myStatePopulation;
@property (strong, nonatomic) NSString * myStateFounded;
@property (strong, nonatomic) NSString * myStateFlag;


@end
